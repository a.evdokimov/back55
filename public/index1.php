<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    $messages = array();
    $errors = array();
    $values = array();


    if (!empty($_COOKIE['save'])) {
      setcookie('save', '', 100000);
      $messages['save'] = 'Спасибо, результаты сохранены.';
  }

  $errors = FALSE;
  $flag=FALSE;
  $super_separated='';
  
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['mail'] = !empty($_COOKIE['mail_error']);
  $errors['year'] = !empty($_COOKIE['year_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['countlimbs'] = !empty($_COOKIE['countlimbs_error']);
  $errors['super'] = !empty($_COOKIE['super_error']);
  $errors['check1'] = !empty($_COOKIE['check1_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  if ($errors['name']) {
    if($_COOKIE['name_error']=='none'){
        setcookie('name_error','',100000);
        $messages['name'] = '<div class="error">Заполните имя.</div>';
    }
    if($_COOKIE['name_error']=='Unacceptable symbols'){
        setcookie('name_error','',100000);
        $messages['name'] = '<div class="error">Недопустимые символы в имени:а-я,А-Я,0-9,\ - _ </div>';
    }
  }
  if ($errors['mail']) {
    if($_COOKIE['mail_error']=='none'){
        setcookie('mail_error','',100000);
        $messages['mail'] = '<div class="error">Укажите почту!</div>';
    }
    if($_COOKIE['mail_error']=='invalid address'){
        setcookie('mail_error','',100000);
        $messages['mail'] = '<div class="error">Почта указана некорректно!Пример:p123@mail.ru</div>';
    }
  }
  if($errors['year']){
    setcookie('year_error','',100000);
    $messages['year'] = '<div class="error">Укажите год рождения!</div>';
}
if($errors['sex']){
        setcookie('sex_error','',100000);
        $messages['sex'] = '<div class="error">Укажите пол!</div>';
}
if ($errors['countlimbs']) {
    setcookie('countlimbs_error','',100000);
    $messages['countlimbs'] = '<div class="error">Укажите количество конечностей!</div>';
}
if($errors['super']) {
    if ($_COOKIE['super_error'] == "none") {
        setcookie('super_error', '', 100000);
        $messages['super'] = '<div class="error">Выберите способности!</div>';
    }
    if ($_COOKIE['super_error'] == "noneselected") {
        setcookie('super_error', '', 100000);
        $messages['super'] = '<div class="error">Что-то пошло не так! Выбирайте способности!!!</div>';
    }
}
if ($errors['biography']) {
        setcookie('biography_error','',100000);
        $messages['biography'] = '<div class="error">Черканите на память!</div>';

}
if($errors['check1']){
    setcookie('check1_error','',100000);
    $messages['check1'] = '<div class="error">Может согласие дадите? Оно обязательно!!!</div>';
}



$values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
$values['mail'] = empty($_COOKIE['mail_value']) ? '' : $_COOKIE['mail_value'];
$values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
$values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
$values['countlimbs'] = empty($_COOKIE['countlimbs_value']) ? '' : $_COOKIE['countlimbs_value'];
$values['super'] = empty($_COOKIE['super_value']) ? '' : $_COOKIE['super_value'];
$values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
$values['check1'] = empty($_COOKIE['check1_value']) ? '' : $_COOKIE['check1_value'];


    include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
    else {
      $errors = FALSE;
      if (empty($_POST['name'])) {
          setcookie('name_error', 'none', time() + 24 * 60 * 60);
          setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
          $errors = TRUE;
      }
     else {
          if (!preg_match("#^[aA-zZ0-9\-_]+$#",$_POST['name'])){
              setcookie('name_error', 'Unacceptable symbols', time() + 24 * 60 * 60);
              setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
              $errors=TRUE;
          }else{
              setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
          }
      }
    if (empty($_POST['mail'])) {
          setcookie('mail_error', 'none', time() + 24 * 60 * 60);
          setcookie('mail_value', $_POST['mail'], time() + 30 * 24 * 60 * 60);
          $errors = TRUE;
      }else{
          if (!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,10})$/i", $_POST['mail'])) {
              setcookie('mail_error', 'invalid address', time() + 24 * 60 * 60);
              setcookie('mail_value', $_POST['mail'], time() + 30 * 24 * 60 * 60);
              $errors = TRUE;
          }else{
              setcookie('mail_value', $_POST['mail'], time() + 30 * 24 * 60 * 60);
          }
      }
    if (empty($_POST['year'])) {
          setcookie('year_error', 'none', time() + 24 * 60 * 60);
          setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60);
          $errors = TRUE;
      }else{
          setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60);
      }
    if (empty($_POST['sex'])) {
          setcookie('sex_error', 'none', time() + 24 * 60 * 60);
          setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
          $errors = TRUE;
      }else{
          setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
      }
    if (empty($_POST['countlimbs'])) {
          setcookie('countlimbs_error', 'none', time() + 24 * 60 * 60);
          setcookie('countlimbs_value', $_POST['countlimbs'], time() + 30 * 24 * 60 * 60);
          $errors = TRUE;
      }else{
          setcookie('countlimbs_value', $_POST['countlimbs'], time() + 30 * 24 * 60 * 60);
      }
    if(!isset($_POST['super'])){
          setcookie('super_error', 'none', time() + 24 * 60 * 60);
          setcookie('super_value', serialize($_POST['super']), time() + 30 * 24 * 60 * 60);
          $errors = TRUE;
      }else{
          $super_mass=$_POST['super'];
          $flag=FALSE;
          for($w=0;$w<count($super_mass);$w++){
              if($super_mass[$w]=="net"){
                  $flag=TRUE;break;
              }
          }
          if($flag && count($super_mass)!=1){
              setcookie('super_error', 'noneselected', time() + 24 * 60 * 60);
              setcookie('super_value',serialize($_POST['super']), time() + 30 * 24 * 60 * 60);
              $errors = TRUE;
          }else{
              setcookie('super_value',serialize($_POST['super']), time() + 30 * 24 * 60 * 60);
          }
      }
    if (empty($_POST['biography'])) {
          setcookie('biography_error', 'none', time() + 24 * 60 * 60);
          setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
          $errors = TRUE;
      }else{
          setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
      }
    if (empty($_POST['check1'])) {
          setcookie('check1_error', 'none', time() + 24 * 60 * 60);
          setcookie('check1_value', $_POST['check1'], time() + 30 * 24 * 60 * 60);
          $errors = TRUE;
      }else{
          setcookie('check1_value', $_POST['check1'], time() + 30 * 24 * 60 * 60);
      }
     if ($errors) {
          header('Location:index1.php');
          exit();
      }
      else {
          setcookie('name_error', '', 100000);setcookie('countlimbs_error', '', 100000);
          setcookie('mail_error', '', 100000);setcookie('super_error', '', 100000);
          setcookie('year_error', '', 100000);setcookie('biography_error', '', 100000);
          setcookie('sex_error', '', 100000);setcookie('check1_error', '', 100000);
      }


      if(!empty($_POST['super'])){
        $super_mass=$_POST['super'];
        for($w=0;$w<count($super_mass);$w++){
            if($flag){
                if($super_mass[$w]!="net")unset($super_mass[$w]);
                $super_separated=implode(' ',$super_mass);
            }else{
                $super_separated=implode(' ',$super_mass);
            }
        }
    }

try{

    $db = new PDO("mysql:host=localhost;dbname=u20937", 'u20937', '6902593', array(PDO::ATTR_PERSISTENT => true));

    $stmt = $db->prepare("INSERT INTO application (name, mail, birth, sex, countlimbs, super, biography,check1) 
    VALUES (:name, :mail, :birth, :sex, :countlimbs,:super,:biography, :check1)");
    $stmt->bindParam(':name', $name_db);
    $stmt->bindParam(':mail', $mail_db);
    $stmt->bindParam(':birth', $year_db);
    $stmt->bindParam(':sex', $sex_db);
    $stmt->bindParam(':countlimbs', $limb_db);
    $stmt->bindParam(':super', $super_db);
    $stmt->bindParam(':biography', $bio_db);
    $stmt->bindParam(':check1', $check1_db);
    $name_db=$_POST["name"];
    $mail_db=$_POST["mail"];
    $year_db=$_POST["year"];
    $sex_db=$_POST["sex"];
    $limb_db=$_POST["countlimbs"];
    $super_db=$super_separated;
    $bio_db=$_POST["biography"];
    $check1_db=$_POST["check1"];
    $stmt->execute();
   

    header("Location: ?save=1");
}
catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
}
setcookie('save', '1');
header('Location: index1.php');}

?>
