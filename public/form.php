<?php
    if($_COOKIE['login']==""){header('Location:index.php');die;}
    echo "Вы вошли как ".$_COOKIE['login'];
?>
<!DOCTYPE html>
<html lang="ru">
	<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Minimal</title>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <link rel="stylesheet" href="style.css">

<script >
  document.addEventListener("DOMContentLoaded", function() { 
    document.querySelectorAll('textarea, input').forEach(function(e) {
        if(e.value === '') e.value = window.sessionStorage.getItem(e.name, e.value);
        e.addEventListener('input', function() {
            window.sessionStorage.setItem(e.name, e.value);
        })
    })

}); 
</script>

	</head>
    <body>
    <div class="wrapper">
    <header id="header" class="header lock-padding">
      <div class="header__container _conteiner">
        <a href="" class="header__logo"></a>
        <a href="#popup" class="popup-link">Форма</a> 
      </div>
    </header>
  </div>
  <div class="popup" id="popup">
  <div class="popup__body">
    <div class="popup__content">
      <a href="##" class="popup__close"></a>
<form action="index.php" accept-charset="UTF-8"  method="POST">
			 <?php
                if (!empty($messages['save'])) {
                    print($messages['save']);
                }
                ?>
				<div id="nam">
                    <?php
                    $ERROR='';
                    $name='';
                    if (!empty($messages['name'])) {
                        print($messages['name']);
                        $ERROR='error';
                    }
                    if(!empty($values['name'])){
                        $name=$values['name'];
                    }
                    ?>
					Name:<input maxlength="25" size="40" name="name" placeholder="First name" class="<?php print $ERROR?>" value="<?php print $name?>">
				</div>
                </br>
				<div id="address">
                    <?php
                    $ERROR='';
                    $mail='';
                    if (!empty($messages['mail'])) {
                        print($messages['mail']);
                        $ERROR='error';
                    }
                    if(!empty($values['mail'])){
                        $mail=$values['mail'];
                    }
                    ?>
					Email:<input name="mail" value="<?php print $mail?>" class="<?php print $ERROR?>" placeholder="mail@inbox.ru">
				</div>
                </br>
            <div id="BIRTHYEAR">
                    <?php
                    $ERROR='';
                    if (!empty($messages['year'])) {
                        print($messages['year']);
                        $ERROR='error';
                    } ?>
	            Year of Birth:
                    <span class="<?php print $ERROR?>">
                        <select name="year" size="1">
                            <?php
                            $select=array(1900-2000=>'',2000-2005=>'',2006-2011=>'',2012-2017=>'',2018-2020=>'');
                            for($s=1900-2000;$s<=2018-2020;$s++){
                                if($values['year']==$s){
                                    $select[$s]='selected';break;
                                }
                            }
                            ?>
                            <option value="">...</option>
							<option value="1900-2000" <?php print $select[1900-2000]?>>1900-2000</option>
                            <option value="2000-2005" <?php print $select[2000-2005]?>>2000-2005</option>
                            <option value="2006-2011" <?php print $select[2006-2011]?>>2006-2011</option>
                            <option value="2012-2017" <?php print $select[2012-2017]?>>2012-2017</option>
                            <option value="2018-2020" <?php print $select[2018-2020]?>>2018-2020</option>
                        </select>
                    </span>
				</div>
                </br>
	
				<div id="SEX">
                    <?php
                    $ERROR='';
                    if (!empty($messages['sex'])) {
                        print($messages['sex']);
                        $ERROR='error';
                    }?>
                Sex:    <span class="<?php print $ERROR?>">
                            <input type="radio" value="M" name="sex"<?php if($values['sex']=='M') {print'checked';}?> >Man
                            <input type="radio" value="F" name="sex"<?php if($values['sex']=='F') {print'checked';}?> >Female
                    </span>
                </div>
                </br>
					<div id="LIMBS">
                    <?php
                    $ERROR='';
                    if (!empty($messages['limbs'])) {
                        print($messages['limbs']);
                        $ERROR='error';
                    }
                    ?>
                    Limbs:<?php
                    $select_limbs=array(1=>'',2=>'',2=>'',3=>'',4=>'');
                    for($s=1;$s<=4;$s++){
                        if($values['countlimbs']==$s){
                            $select_limbs[$s]='checked';break;
                        }
                    }
                    ?>
                    <span class="<?php print $ERROR?>">
                        <input type="radio" value="1" name="countlimbs" <?php print $select_limbs[1]?>>1
                        <input type="radio" value="2" name="countlimbs" <?php print $select_limbs[2]?>>2
                        <input type="radio" value="3" name="countlimbs" <?php print $select_limbs[3]?>>3
                        <input type="radio" value="4" name="countlimbs" <?php print $select_limbs[4]?>>4
                    </span>
                </div>
                </br>
					<div id="SUPERPOWERS" >
                    <?php
                    $ERROR='';
                    if(!empty($messages['super'])){
                        print($messages['super']);
                        $ERROR='error';
                    }?>
                    <span >
                        Superpowers:</br>
                        <?php
                         if(!empty($values['super'])){
                             $flag=FALSE;
                             $SUPER_PROVERKA = array("net" =>"", "godmod" =>"", "levitation" =>"", "unvisibility" =>"", "telekinesis" =>"", "extrasensory" =>"");
                             $SUPER = unserialize($values['super']);
                            if(!empty($SUPER))foreach ($SUPER as $E){
                                if($E=="net"){
                                    $SUPER_PROVERKA["net"]="selected";
                                $flag=TRUE;break;}
                            }
                            if(!empty($SUPER))
                                    if(!$flag){
                                        foreach ($SUPER as $T){
                                            $SUPER_PROVERKA["$T"]="selected";
                                        }
                                    }
                         }
                        ?>
                        <select id="sposobnost" name="super[]" multiple="multiple" size="3" class="<?php print $ERROR?>">
                            <option value="net" <?php if(!empty($values['super'])) print $SUPER_PROVERKA["net"]?>>None</option>
                            <option value="godmod"<?php if(!empty($values['super'])) print $SUPER_PROVERKA["godmod"]?> >GodMode</option>
                            <option value="levitation"<?php if(!empty($values['super'])) print $SUPER_PROVERKA["levitation"]?> >Levitation</option>
                            <option value="unvisibility"<?php if(!empty($values['super'])) print $SUPER_PROVERKA["unvisibility"]?> >Invisibility</option>
                            <option value="telekinesis"<?php if(!empty($values['super'])) print $SUPER_PROVERKA["telekinesis"]?> >Telekinesis</option>
                            <option value="extrasensory"<?php if(!empty($values['super'])) print $SUPER_PROVERKA["extrasensory"]?> >Extrasensory</option>
                        </select>
                    </span>
                </div>
                </br>
						<div id="biography">
                        <?php
                        $ERROR='';
                        if (!empty($messages['biography'])) {
                            print($messages['biography']);
                            $ERROR='error';
                        }
                        ?>
                        <p class="<?php print $ERROR?>" >
                            <textarea cols="45" style="width:300px; height:150px;" name="biography" placeholder="Here is your biography..."><?php if($values['biography']){print $values['biography'];} ?></textarea>
                        </p>
                    </div>
                </br>
                    <div id="Сheck1"  >
                    <?php
                    $ERROR='';
                    if (!empty($messages['check1'])) {
                        print($messages['check1']);
                        $ERROR='error';
                    }
                    ?>
                    <span class="<?php print $ERROR?>" >Contract introduced
					    <input type="checkbox" name="check1"  value="yes" <?php if($values['check1']=='yes') {print'checked';}?> >
                    </span>
                </div>
                </br>
             <input type="submit" value="Отправить"> 
	         </form>
    </div>
  </div>
</div>
<script src="popup.js"></script>
 </body>
 </html>

