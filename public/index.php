<!DOCTYPE html>

<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Minimal</title>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <link rel="stylesheet" href="style.css">
    
<script >
  document.addEventListener("DOMContentLoaded", function() { 
    document.querySelectorAll('textarea, input').forEach(function(e) {
        if(e.value === '') e.value = window.sessionStorage.getItem(e.name, e.value);
        e.addEventListener('input', function() {
            window.sessionStorage.setItem(e.name, e.value);
        })
    })

}); 
</script>

  </head>
     
<body>
  <div class="wrapper">
    <header id="header" class="header lock-padding">
      <div class="header__container _conteiner">
        <a href="" class="header__logo"></a>
        <a href="#popup" class="popup-link">Логин</a> 
      </div>
    </header>
  </div>
 
<div class="popup" id="popup">
  <div class="popup__body">
    <div class="popup__content">
      <a href="##" class="popup__close"></a>
      <form method="POST" action="enter.php">
        <input type="text" placeholder="Логин" name="login">
        <input type="password" placeholder="Пароль" name="pass">
		<input type="submit" value ="Войти">	
    </form> 
    </div>
  </div>
</div>
<script src="popup.js"></script>

</body>

</html>
